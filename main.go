package main

import (
  "fmt"
  "os"
  "os/exec"
  "bufio"
  "strings"
  "math/rand"
  "strconv"
  "time"
)

func welcome() {
  fmt.Println("|====================|")
  fmt.Println("|[●▪▪●]| MATH HELPER |")
  fmt.Println("|====================|")
}

func math(e string, r, c, w int) (bool, int, int) {
  fmt.Printf(e)
  result:for {
    s := selected()
    if s == "q" || s == "Q" {
      return false, c, w
    }

    resInt, _ := strconv.ParseInt(s, 10, 0)
    if r == int(resInt) {
      c++
      wave(c - w)

      break result
    }

    w++
    fmt.Println(" NO :(")

    fmt.Printf(e)
  }

  return true, c, w
}

func wave(l int) {

  wave := strings.Repeat("_.~\"~.", l)
  wave = wave + "_"

  fmt.Println(wave)
}

func substr(max, min, c, w int) (int, int) {
  a, b := rand.Intn(max), rand.Intn(max)
  for {
    if a >= min && a - b >= 0 {
      break
    }

    a = rand.Intn(max)
    b = rand.Intn(max)
  }

  r := a - b
  next, c, w := math(fmt.Sprintf("%d - %d = ", a, b), r, c, w)
  if next {
    return substr(max, min, c, w)
  }

  return c, w
}

func substrTwo(max, min, c, w int) (int, int) {
  a, b, c := rand.Intn(max), rand.Intn(max), rand.Intn(max)
  for {
    if a >= min && a - b - c >= 0 {
      break
    }

    a = rand.Intn(max)
    b = rand.Intn(max)
    c = rand.Intn(max)
  }

  r := a - b - c
  next, c, w := math(fmt.Sprintf("%d - %d - %d = ", a, b, c), r, c, w)

  if next {
    return substrTwo(max, min, c, w)
  }

  return c, w
}

func add(max, min, c, w int) (int, int) {
  a, b := rand.Intn(max), rand.Intn(max)
  for {
    if a >= min && a + b <= max {
      break
    }

    a = rand.Intn(max)
    b = rand.Intn(max)
  }

  r := a + b
  next, c, w := math(fmt.Sprintf("%d + %d = ", a, b), r, c, w)
  if next {
    return add(max, min, c, w)
  }

  return c, w
}

func addTwo(max, min, c, w int) (int, int) {
  a, b, c := rand.Intn(max), rand.Intn(max), rand.Intn(max)
  for {
    if a >= min && a + b + c <= max {
      break
    }

    a = rand.Intn(max)
    b = rand.Intn(max)
    c = rand.Intn(max)
  }

  r := a + b + c
  next, c, w := math(fmt.Sprintf("%d + %d + %d = ", a, b, c), r, c, w)
  if next {
    return addTwo(max, min, c, w)
  }

  return c, w
}

func multi(base []int, mu, c, w int) (int, int) {
  b := rand.Intn(mu)
  a := base[rand.Intn(len(base))]

  r := a * b
  next, c, w := math(fmt.Sprintf("%d * %d = ", a, b), r, c, w)
  if next {
    return multi(base, mu, c, w)
  }

  return c, w
}

func division(base []int, mu, c, w int) (int, int) {
  b := rand.Intn(mu)
  a := base[rand.Intn(len(base))]
  r := a * b
  next, c, w := math(fmt.Sprintf("%d / %d = ", r, a), b, c, w)

  if next {
    return division(base, mu, c, w)
  }

  return c, w
}

func menu() {
  fmt.Println("\nSELECT YOUR OPTION")
  fmt.Println(" 1  - Addition up to 10")
  fmt.Println(" 2  - Addition up to 100")
  fmt.Println(" 3  - Addition up to 1000")
  fmt.Println(" 4  - Addition up to 10000")
  fmt.Println(" 5  - Addition up to 100000")
  fmt.Println(" 6  - Addition up to 1000000")
  fmt.Println(" 7  - Substraction up to 10")
  fmt.Println(" 8  - Substraction up to 100")
  fmt.Println(" 9  - Substraction up to 1000")
  fmt.Println(" 10  - Substraction up to 10000")
  fmt.Println(" 11  - Substraction up to 100000")
  fmt.Println(" 12  - Substraction up to 1000000")
  fmt.Println(" 13  - Multiplication 1, 2, 3, 4, 5")
  fmt.Println(" 14  - Multiplication 6, 7, 8, 9, 10")
  fmt.Println(" 15  - Multiplication 11, 12, 13, 14, 15")
  fmt.Println(" 16 - Multiplication 16, 17, 18, 19, 20")
  fmt.Println(" 17 - Division 1, 2, 3, 4, 5")
  fmt.Println(" 18 - Division 6, 7, 8, 9, 10")
  fmt.Println(" 19 - Division 11, 12, 13, 14, 15")
  fmt.Println(" 20 - Division 16, 17, 18, 19, 20")
  fmt.Println(" 21  - 2 Addition up to 10")
  fmt.Println(" 22  - 2 Addition up to 100")
  fmt.Println(" 23  - 2 Addition up to 1000")
  fmt.Println(" 24  - 2 Addition up to 10000")
  fmt.Println(" 25  - 2 Addition up to 100000")
  fmt.Println(" 26  - 2 Addition up to 1000000")
  fmt.Println(" 27  - 2 Substraction up to 10")
  fmt.Println(" 28  - 2 Substraction up to 100")
  fmt.Println(" 29  - 2 Substraction up to 1000")
  fmt.Println(" 30  - 2 Substraction up to 10000")
  fmt.Println(" 31  - 2 Substraction up to 100000")
  fmt.Println(" 32  - 2 Substraction up to 1000000")
  fmt.Println(" Q - Quit")
  fmt.Print("Your choice: ")
}

func finish(c, w int) {
  fmt.Println("\nYOUR RESULTS")
  fmt.Println("--------------")
  fmt.Printf("- Correct  : %d\n", c)
  fmt.Printf("- Incorrect: %d\n", w)


  fmt.Println("\nTHANK YOU FOR LEARNING WITH US")
}

func selected() string {
  reader := bufio.NewReader(os.Stdin)
  v, err := reader.ReadString('\n')
  if err != nil {
    panic(err)
  }

  return strings.Replace(v, "\n", "", -1)
}

func clearScreen() {
  cmd := exec.Command("clear")
  cmd.Stdout = os.Stdout
  cmd.Run()
}

func main() {
  clearScreen()
  rand.Seed(time.Now().UnixNano())
  welcome()
  menu()

  sel := selected()
  clearScreen()
  welcome()
  c, w := 0, 0
  switch sel {
    case "1":
      c, w = add(10, 0, c, w)
    case "2":
      c, w = add(100, 10, c, w)
    case "3":
      c, w = add(1000, 100, c, w)
    case "4":
      c, w = add(10000, 1000, c, w)
    case "5":
      c, w = add(100000, 10000, c, w)
    case "6":
      c, w = add(1000000, 100000, c, w)
    case "7":
      c, w = substr(10, 0, c, w)
    case "8":
      c, w = substr(100, 10, c, w)
    case "9":
      c, w = substr(1000, 100, c, w)
    case "10":
      c, w = substr(10000, 1000, c, w)
    case "11":
      c, w = substr(100000, 10000, c, w)
    case "12":
      c, w = substr(1000000, 100000, c, w)
    case "13":
      c, w = multi([]int{1, 2, 3, 4, 5}, 10, c, w)
    case "14":
      c, w = multi([]int{6, 7, 8, 9, 10}, 10, c, w)
    case "15":
      c, w = multi([]int{11, 12, 13, 14, 15}, 20, c, w)
    case "16":
      c, w = multi([]int{16, 17, 18, 19, 20}, 20, c, w)
    case "17":
      c, w = division([]int{1, 2, 3, 4, 5}, 10, c, w)
    case "18":
      c, w = division([]int{6, 7, 8, 9, 10}, 10, c, w)
    case "19":
      c, w = division([]int{11, 12, 13, 14, 15}, 20, c, w)
    case "20":
      c, w = division([]int{16, 17, 18, 19, 20}, 20, c, w)
    case "21":
      c, w = addTwo(10, 0, c, w)
    case "22":
      c, w = addTwo(100, 10, c, w)
    case "23":
      c, w = addTwo(1000, 100, c, w)
    case "24":
      c, w = addTwo(10000, 1000, c, w)
    case "25":
      c, w = addTwo(100000, 10000, c, w)
    case "26":
      c, w = addTwo(1000000, 10000, c, w)
    case "27":
      c, w = substrTwo(10, 0, c, w)
    case "28":
      c, w = substrTwo(100, 10, c, w)
    case "29":
      c, w = substrTwo(1000, 100, c, w)
    case "30":
      c, w = substrTwo(10000, 1000, c, w)
    case "31":
      c, w = substrTwo(100000, 10000, c, w)
    case "32":
      c, w = substrTwo(1000000, 10000, c, w)
  }

  finish(c, w)
}
